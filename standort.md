---
layout: page
title: Standort
permalink: /standort/
---

Das Hacklabor befindet sich im Haus 1 des [TGZ](http://tgz-mv.de) in Schwerin.  
Die Adresse lautet:

Hagenower Straße 73  
19061 Schwerin

53.6011, 11.4183

Adresse öffnen in [OSM](http://osm.org/go/0NAH7ckyf--?layers=HN&m=&node=3948563704)/[Google](https://goo.gl/maps/HgK3iCKfRwn).

## Parkplätze

Auf dem Gelände des TGZ befinden sich zahlreiche Parkplätze für Autos. Dein Fahrrad kannst du direkt vor dem Hacklabor oder gegenüber im Fahrradständer abstellen.

## ÖPNV

Das TGZ erreichst du prima per Bus oder Straßenbahn. Wir empfehlen folgende Haltestellen:

-   Technologiezentrum (Bus 7) - [Fahrpläne](http://nahverkehr-schwerin.de/fahrplan/fahrplan_2016-2017/aushang/#T)
-   alternativ: Rosenstraße (Bus 7) - [Fahrpläne](http://nahverkehr-schwerin.de/fahrplan/fahrplan_2016-2017/aushang/#R)
-   Gartenstadt (Tram 1,2,4, Bus 19) - [Fahrpläne](http://nahverkehr-schwerin.de/fahrplan/fahrplan_2016-2017/aushang/#G)

### Abfahrtsmonitor

<ul id="listefahrten">
	<li>Bitte warte kurz oder aktiviere Javascript :)</li>
</ul>

<script src="/assets/js/mustache.min.js"></script>
{% raw  %}
<script id="fahrtTpl" type="text/template">
	{{#.}}
	<li>
		<span class="time">{{UhrzeitAusgabe}}</span> - <span class="stop">{{Haltestelle}}</span><br>
                <span class="type">{{Typ}}</span> <span class="number">{{Nummer}}</span> - <span class="dest">{{Ziel}}</span>		
	</li>
	{{/.}}
</script>
{% endraw  %}

### Fahrplanauskunft

-   [Hinfahrt](http://80.146.180.107/vmv/XSLT_TRIP_REQUEST2?language=de&sessionID=0&itdLPxx_transpCompany=vmv&execInst=readOnly&lineRestriction=400&ptOptionsActive=1&place_destination=Schwerin&name_destination=Hagenower%20Straße&type_destination=address&)
-   [Rückfahrt](http://80.146.180.107/vmv/XSLT_TRIP_REQUEST2?language=de&sessionID=0&itdLPxx_transpCompany=vmv&execInst=readOnly&lineRestriction=400&ptOptionsActive=1&place_origin=Schwerin&name_origin=Hagenower%20Strasse%2075&type_origin=address)
