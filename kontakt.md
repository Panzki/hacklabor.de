---
layout: page
title: Kontakt
permalink: /kontakt/
---

{{ site.email }}

Hackspace Schwerin e.V.  
Hagenower Straße 73  
19061 Schwerin

\+49 385 3993 - 365

<h2 id="mailinglisten">Mailinglisten</h2>

Für Ankündigungen gibt es eine Announcements-Mailingliste. Das Abonnement erfolgt
mit einer Mail an [announcements-subscribe@hacklabor.de](mailto:announcements-subscribe@hacklabor.de).

Vereinsmitglieder können sich auf der Mitglieder-Mailingliste austauschen. Das Abonnement erfolgt
mit einer Mail an [members-subscribe@hacklabor.de](mailto:members-subscribe@hacklabor.de).

## Gated Communities

[Twitter](https://twitter.com/hacklabor)  
[Facebook](https://facebook.com/hacklabor)

{% include map.html %}
