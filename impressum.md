---
layout: page
title: Impressum
permalink: /impressum/
---

Hackspace Schwerin e.V.  
Hagenower Straße 73  
19061 Schwerin

E-Mail: {{ site.email }}

**Vertreten durch:**

-   Murdoc Bates
-   Matthias Manow
-   Michael Milz

**Registereintrag:**  
Registergericht: Amtsgericht Schwerin  
Registernummer: VR 10234

**Verantwortlich für den Inhalt** (gem. § 55 Abs. 2 RStV):  
Die oben genannten Vertreter

## Hinweis auf EU-Streitschlichtung

Zur außergerichtlichen Beilegung von verbraucherrechtlichen Streitigkeiten hat die Europäische Union eine Online-Plattform (“OS-Plattform”) eingerichtet, an die Sie sich wenden können. Die Plattform finden Sie unter <a href="http://ec.europa.eu/consumers/odr/">http&#x3A;//ec.europa.eu/consumers/odr/</a>. Unsere E-Mail-Adresse lautet: {{ site.email }}

Telefon: 01723101862 (Michael Milz)
