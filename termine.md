---
layout: page
title: Termine
permalink: /termine/
---

<div class="row">
<div class="col-md-8">
  
  {% include open-and-infos.html %}
  
  <ul>
  {% for item in site.data.calendarmore.items %}
    <li>
        {{ item.start.dateTime | date: '%d.%m.%Y - %H:%M' }}<br>
        <small>{{ item.summary }}</small>
    </li>
  {% endfor %}
  </ul>
  
</div>
<div class="col-md-4">
  
  
</div>
</div>

{% include map.html %}
