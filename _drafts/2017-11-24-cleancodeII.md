---
title: "Clean Code(r) Teil 2 - oder: Vom Frickeln zum Entwickeln."
tags: code, cleancode
author: moe
---

[![SomeCode](/assets/blog/CleanCodeCode2.png)](https://www.facebook.com/events/2007446456154567)


Jeder kennt es: Eben schnell mal was proggen (programmieren), was zusammenbasteln und tadaaaah: Es läuft! 
Aber: Es läuft gerade so und der Code sieht aus wie ein modernes ASCII-Art Kunstwerk. 
Nur: Wie soll denn "Clean Code" aussehen und wie wird man vom ambitionierten Programmierer (Frickler) zu einem professionellen Entwickler? 
Anhand eingängiger (negativ-)Beispiele wird gezeigt, was eigentlich typische Probleme sind, warum und was man dagegen tun kann. 
Pragmatisch werden Tools, Methoden und Vorgehensmodelle vorgestellt und auch Tipps für den Entwickleralltag vermittelt. 

__Dies ist der 2. Teil der Vortragsreihe.__ 

Angestrebt werden 2x 45min praktisches Arbeiten am Rechner mit Pause zwischendurch, abhängig vom Publikum.


#### Wann?
30.11.2017 - ab 19 Uhr

#### Wo?
Hacklabor Schwerin
Hagenower Straße 73
19061 Schwerin

#### Was muss ich mitbringen?
* ein Laptop mit Webbrowser (Code-Übungen laufen online), Leihgeräte sind vorhanded
* Spaß am Coden
* keine oder beliebige Programmierkenntnisse
* Gute Laune


#### Vortragender 

Christian Dähn / [https://github.com/3dh-de](https://github.com/3dh-de)


#### Eintritt
Die Veranstaltung ist kostenlos. Bring bitte trotzdem etwas Kleingeld für das eine oder andere mit.


#### Referals

[https://github.com/3dh-de/clean-code-r/blob/master/clean-code-r_inhalt.md](https://github.com/3dh-de/clean-code-r/blob/master/clean-code-r_inhalt.md)


Wir freuen uns auf Dich!

PS: [So kommst du zu uns](/standort/)
