---
layout: default
---

<p class="jumbo">Das Hacklabor ist ein <span class="important">Schweriner Hackspace</span> für Menschen
  mit Interesse am kreativen Umgang mit Technik. Hier geht es um Hardware, Software, gesellschaftliche 
  Themen und das Drumherum.</p>

<a href="/assets/img/hacklabor.vr.jpg">![Ein Foto vom Labor](/assets/img/hacklabor.vr.small.jpg)</a>

<div class="row blog">
<div class="col-md-8">
  <h1>Aus dem Labor</h1>

  <div class="posts">
    {% for post in site.categories.sticky limit:1 %}
      <article class="post">

        <h2><a href="{{ site.baseurl }}{{ post.url }}">{{ post.title }}</a></h2>

        <div class="entry">
          {{ post.excerpt }}
        </div>

        <a href="{{ site.baseurl }}{{ post.url }}" class="read-more">weiterlesen</a>
      </article>

    {% endfor %}

    {% for post in site.posts limit:5 %}
      {% if post.categories contains "sticky" %}
      {% else %}

      <article class="post">

        <h2><a href="{{ site.baseurl }}{{ post.url }}">{{ post.title }}</a></h2>

        <div class="entry">
          {{ post.excerpt }}
        </div>

        <a href="{{ site.baseurl }}{{ post.url }}" class="read-more">weiterlesen</a>
      </article>

      {% endif %}
    {% endfor %}

  </div>
</div>
<div class="col-md-4">
  <h1>Termine</h1>
  <ul>
  {% for item in site.data.calendar.items %}
    <li>
        {{ item.summary }}<br>
        <small>{{ item.start.dateTime | date: '%d.%m.%Y - %H:%M' }}</small>
    </li>
  {% endfor %}
  </ul>
  
  {% include open-and-infos.html %}
  
</div>
</div>

{% include map.html %}
