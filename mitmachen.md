---
layout: page
title: Mitmachen
permalink: /mitmachen/
---

{% include open-and-infos.html %}

![Ein Foto vom Labor](/assets/img/hacklabor.vr.small.jpg)

Hackspace Schwerin e.V.  
Hagenower Straße 73  
19061 Schwerin

{{ site.email }}

[Twitter](https://twitter.com/hacklabor)  
[Facebook](https://facebook.com/hacklabor)

{% include mitglied-werden.html %}

{% include map.html %}
