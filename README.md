# Website vom Hacklabor Schwerin

Die Website basiert auf Jekyll. Unter [http://jekyllrb.com](http://jekyllrb.com/docs/home/) gibt es eine ausführliche Dokumentation. 

## Blogbeiträge

Veröffentlichte Blogbeiträge befinden sich im Ordner `_posts`. Der Dateiname setzt 
sich aus `YYYY-MM-DD-titel.md`zusammen. Das Datum im Dateinamen ist das Datum der 
Veröffentlichung. Der Titel im Dateinamen sollte kleingeschrieben sein und außer
Bindestrichen keine Sonderzeichen enthalten. 

Der fertige Beitrag ist dann unter `/YYYY/MM/titel/` errreichbar.

Am besten kopierst du dir den Inhalt einer vorhandene Datei, legst eine neue Datei 
im `_drafts` Ordner und passt den kopierten Inhalt an. Dateien in diesem Ordner 
werden nicht veröffentlicht. So kann man in Ruhe an seinem Beitrag schreiben. Wenn 
du unsicher bist, lass jemanden auf deine Datei im `_drafts` Ordner draufschauen.  

Für die Formatierung kannst du Markdown und HTML nutzen.

Zur Veröffentlichung verschiebe deine Datei aus dem `_drafts` Ordner in den
`_posts` Ordner. Die Seite wird danach automatisch zusammengebaut. Das dauert
in der Regel wenige Minuten. Dann ist dein Beitrag unter hacklabor.de zu sehen.

Wenn du deinen Posts auf der Startseite oben halten möchtest, fügst du im Kopf
die Zeil `categories: [sticky]` ein. Der jeweils aktuellste mit dieser Anweisung
erscheint dann dort. Gibt es zwei aktuelle Beiträge mit dieser Anweisung erscheint
der ältere gar nicht auf der Startseite. Du musst dann die Anweisung wieder entfernen.

Youtube Videos können mit `{% youtube YOUTUBE_ID %}` innerhalb des Beitrages eingefügt werden.

## Dateien

Dateien für deinen Blogbeitrag wie zum Beispiel Bilder, kannst du im `assets` 
Ordner ablegen. Am Besten legst du einen Unterordner für deinen Beitrag an. 
Die Bilder sollten nicht unnötig groß sein und mit imageoptim oder einem 
vergleichbaren Programm optimiert sein.

## Projekte

Deine Projekte kannst du auf mindestens zwei Arten auf die Seite bringen.

### Öffentliches Repository auf gitlab.com

Alle öffentlichen Repos unsere Gruppe auf gitlab.com werden automatisch auf der 
Website veröffentlicht. Wenn du das Wiki aktiviert lässt, wird es ebenfalls verlinkt.

### Blogbeitrag

Im 1. Schritt legst du einen Blogbeitrag an. Das wird weiter oben beschrieben.
Danach kopierst du in `_data/projekte.yml` einen Abschnitt und passt ihn für
dein Projekt an.

**Beachte** bitte, dass du in YAML Dateien Leerzeichen statt Tabs verwenden musst.

## CSS Änderungen

Das CSS entsteht aus den `.scss` Dateien im `assets/sass` Ordner. **Bitte nimm keine Änderungen an des CSS Dateien vor.** Deine Änderungen dort werden garantiert wieder überschrieben.

### SCSS zu CSS

Dafür benötigst du nodejs auf deinem Rechner. Mit `npm install` werden (hoffentlich) alle Abhängigkeiten installiert. Danach kannst du mit `gulp sass` das SCSS kompilieren. Arbeitest du permanent an den SCSS Dateien kannst du auch `gulp watch` aufrufen. Dann findet die Umwandlung sofort bei Änderung einer SCSS Datei statt.

## Änderungen testen

Jekyll basiert auf Ruby. Du brauchst also ein halbwegs aktuelles Ruby auf deinem Rechner. Hast du das jekyll Gem noch nicht installiert, musst du `gem install bundler && bundle install` ausführen. Danach kannst du mit `jekyll serve` einen lokalen Webserver starten, der dir die aktuelle Website anzeigt.

### Das funktioniert lokal nicht

Für die Ausgabe der Termine und Repos auf gitlab.com werden API Keys benötigt. 
Die sind nicht im Code sondern als Variablen in gitlab hinterlegt. 

## Änderungen veröffentlichen

Durch Schieben des `pages` Zweiges in das gitlab.com Repository, werden Änderungen zeitnah veröffentlicht.

Wenn deine Änderungen nicht erscheinen, leere deinen Browsercache. Danach kannst du in der [Build-Pipeline](https://gitlab.com/hacklabor/hacklabor.de/pipelines) schauen ob der Build mit deinen Änderungen eventuell fehlgeschlagen ist. 

## SPACEAPI

Siehe https://gitlab.com/hacklabor/spaceapi