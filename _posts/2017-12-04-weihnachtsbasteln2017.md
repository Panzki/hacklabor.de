---
title: Weihnachtsbasteln 2017
tags: Weihnachten, Löten
author: micha
---
![Weihnachtsbaum](/assets/blog/weihnachten2017.jpg)

Das Hacklabor lädt am kommenden Samstag (09.12.) ab 13:37 Uhr zum traditionellen “Weihnachtsbasteln” ein. Wir haben wieder 
spannende Lötkits aufgetrieben. Mit Würfeln, Robots, die Linien folgen können 
und Weihnachtsbäumen ist für Anfänger bis Fortgeschrittene etwas dabei.

Zwischendurch könnt ihr mit Ausstechformen aus dem 3D-Drucker einzigartige 
Plätzen backen.

Die Veranstaltung ist kostenlos. Für die Lötkits erbitten wir eine Spende.


Wir freuen uns auf Euch!
Euer Team vom Hacklabor

PS: [So kommst du zu uns](/standort/)
