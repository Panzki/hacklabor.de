---
title: Kaffeeklatsch
tags: Gesellschaft, event, Diskussion, oeffentlich
author: Thomas
---
![](/assets/blog/Kaffeeklatsch/Kaffee_und_Kuchen.jpg)
Sonntag von 14.00 bis 16.00 Uhr Kaffeeklatsch. Mit Zitronen Kuchen, Kaffee und/oder Tee.

Wir freuen uns auf Dich!

PS: [So kommst du zu uns](/standort/)