---
title: Hacktoberfest im Hacklabor - Social Coding mit Kaffee und Kuchen
tags: hacktoberfest
author: felix
---

[![](https://nyc3.digitaloceanspaces.com/hacktoberfest/Hacktoberfest17-Email-01.png)](https://hacktoberfest.digitalocean.com/)


Im Oktober 2017 organisieren DigitalOcean und Github das Hacktoberfest.
Wir vom Hacklabor wollen im Rahmen des Hacktoberfests am **28.10.2017** gemeinsam Open-Source-Projekte unterstützen.

Zur Teilnahme eingeladen sind alle Entwickler, egal ob Anfänger oder Veteran.
Neben jeder Menge Karma gibt es auch ein limitiertes T-Shirt zu gewinnen - einzige Bedinung: vier Pull-Requests.

#### Was ist das Hacktoberfest?
DigitalOcean und Github würdigen einen Monat lang Open-Source-Software. Ziel ist es, Unterstützer zu finden und Projekte voran zu bringen.
Regelmäßig profitieren wir von Open-Source-Software wie Firefox. Dies ist die Einladung, einem oder mehreren Projekten etwas zurück zugeben.
Beachte: Kein Feature oder Bugfix ist zu klein, um am Hacktoberfest teilzunehmen.

#### Wann?
28.10.2017 - ab 14 Uhr

#### Wo?
Hacklabor Schwerin
Hagenower Straße 73
19061 Schwerin

#### Was muss ich mitbringen?
* Gute Laune
* Spaß am Coden
* idealerweise deinen Laptop (optional; Wir haben auch welche vor Ort).

Solltest Du noch keinen Github-Account haben, ist das Hacktoberfest der ideale Zeitpunkt, um Github, git und Pull-Requests kennen zu lernen.

#### Was muss ich können?
Du solltest mindestens eine Programmiersprache in seinen Grundzügen beherrschen

#### Du hast bereits vier Pull-Requests auf Github gemacht?
Kein Problem! Komm trotzdem vorbei und hilf den Anderen bei ihren Issues. Das gibt richtig Karmapunkte ;-)

#### Du willst mehr erfahren?
Komm vorbei. Wir sind jeden Dienstag und Freitag im Hacklabor und tauschen uns gerne mit dir aus. Du kannst auch alles zum Hacktoberfest nochmal bei DigitalOcean nachlesen. _[Hier klicken](https://hacktoberfest.digitalocean.com/)_

Wir freuen uns auf Dich!

PS: [So kommst du zu uns](/standort/)
