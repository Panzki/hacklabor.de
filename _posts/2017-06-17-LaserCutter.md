---
title: Lasercutter IBN
tags: jedem der das Projekt vorantreibt....
author: Thomas
---
# Veranstaltung abgesagt.
![](https://i.imgur.com/j6I3u88.jpg)
Wer auch möchte das der Lasercutter endlich seine Arbeit aufnimmt.......

Ist herzlichst Willkommen, am Samstag den 24.06 um 0800 Uhr, tatkräftig bei der Umsetzung Unterstützung zu leisten.


Aufgaben

- Tausch der Steuerelektronik
- Inbetriebnahme
- Herstellen eines halbwegs akzeptablen Kühlsystems (Vorschläge willkommen)
- Test Cut

Arbeitsaufwand ca. 8h

für Verpflegung ist gesorgt....!
![](https://i.imgur.com/rF7t0IJ.jpg)


Wir freuen uns auf Dich!

PS: [So kommst du zu uns](/standort/)

