---
title: Neues Leben für die Feuerkisten
tags: firewall
author: techy
---

Der Hackspace-Verein Schwerin hatte nun einen Raum, das heutige Hacklabor, doch mit dem Internet sah es erstmal Mau aus. Es gab Übergangslösungen. Doch erst, als der Speedtest an der neuen, von Planet-IC zur Verfügung gestellten, Standleitung den Zeiger ausschlagen ließ, war die Begeisterung groß. Wir wollten schnell alles einrichten, doch im Sinnen des Rates „Use your head to save your feet.“ haben wir erstmal angefangen uns einen Plan zu machen, um nachher nicht alles über den Haufen werfen zu müssen. Da uns einfaches, aber umfangreiches, Management des Netzwerkes wichtig ist, wollten wir mit der Schlüsselkomponente „Firewall“ anfangen. 

![Bild der geöffneten Firebox](/assets/blog/firewall/2016-offene-firebox.jpg)

Wir hatten glücklicherweise schon zwei WatchGuard Fireboxes X750e mit einem 1,3 Ghz Celeron und 512MB DDR2 Ram bekommen. Diese sind zwar schon etwas betagt, doch kann man mit den jeweils acht Gigabit-Ports schon etwas anfangen. Der Throughput der Firewall beträgt 1 Gbps und bei VPNs 260 Mbps mit originalem OS. Für die Firewall werden zwar offiziell keine Updates mehr bereitgestellt, doch hielt uns das nicht davon ab, das beste aus den Kisten herauszuholen. 

Wir haben uns eine Auswahl an alternativen Betriebssystem herausgesucht. Bevor wir uns aber weiter mit der Software auseinandersetzen wurde erstmal die Hardware genau inspiziert. Es resultierte ein Fund von Keyboard- und Monitor-Headern, die genutzt werden wollten. Also Bastelkiste raus und Adapter basteln. Eine Viertelstunde später, konnten wir uns auf dem Monitor durch das BIOS klicken. 

Achja, weil die USB-ICs nicht bestückt waren, haben wir unsere Installationsmöglichkeiten mit einem DVD-Laufwerk erweitert. Nun schien alles startbereit, wir hatten eine Installations-CD und eine 16GB (ursprünglich 128MB) Compact-Flash-Card eingelegt und freuten uns auf die Zielgerade. Es ging leider nicht ganz so schnell. Wir probierten verschiedene Installation, doch schien es immer an dem Speicher-Medium zu klemmen. Kurz gegoogelt: das Board unterstützt nur bis zu 4GB und das nur mit einem BIOS-Mod. Okay, also mit FreeDOS auf der 128MB-Karte mit dem Befehl `awdflash x750eb7.bin /py /sn /cc /e` das gemoddete BIOS geflasht, nachdem das Alte gebackuped war. Wir stellten nun den IDE-Mode auf CHS mit 2 Heads und waren zuversichtlich, da die neue 4GB CF-Card von den Installern erkannt wurde. Leider bekamen wir immer wieder die Fehlermeldung `ata0: DMA limited to UDMA33, controller found non-ATA66 cable`. Mh, da muss wohl was mit dem Speicher nicht stimmen. Kurz gegoogelt: pfSense, welches wir in der embedded-nano-Version 2.3 probierten, hat seit 2.2 seinen DMA-Zugriff standardmäßig nicht mehr deaktiviert. Gut, daraufhin haben wir während des Bootens dieses Feature deaktiviert – Es lebte wieder! 

Nachdem wir nach kurzer Konfiguration auf das Webinterface zugreifen konnten, waren wir begeistert. Nach einer Grundkonfiguration schlossen wir die Firewall und mussten uns gleich für die kurzzeite Downtime ein Gestöhne abholen. :wink:

Erfolg! Die Firewall läuft. Es steht nun noch ein kleines Lüfter- und RAM-Upgrade an und dann darf sich unsere Firewall im Langzeittest beweisen.
