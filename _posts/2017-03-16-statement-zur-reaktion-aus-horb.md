---
title: Fake News, Sensationshascherei und Nähe zu Kriminellen?
tags: 102
author: Vorstand
---
In einem Artikel des Schwarzwälder Boten unterstellt die Stadt Horb dem Hacklabor Fake News, Sensationshascherei und Nähe zu Kriminellen. Wie kam es dazu?

Die Mitglieder des Vereins beschäftigen sich neben vielen anderen Themen auch mit Sicherheit von Diensten im Internet. Dabei fand ein Mitglied über die Suchmaschine [Shodan](https://www.shodan.io/explore) die öffentlich erreichbare Adresse und den Port zur Steuerung einer Industrieanlage sowie eine dazugehörige Website. Die Website lieferte zahlreiche Messwerte und Angaben zum Betreiber der Anlage.
Diese Informationen und die offen zugängliche Steuerung wurden vom Hackspace Schwerin e.V. als Sicherheitsrisiko eingestuft und entschieden, die Verantwortlichen umgehend zu informieren. Ein Vorstandsmitglied hat am 14.03.2017 den Zuständigen und dessen Sekretariat per E-Mail detailiert auf das Sicherheitsrisiko und eine Möglichkeit zur Beseitigung der Sicherheitslücke hingewiesen.

Da das Thema Sicherheit von Industrieanlagen - insbesondere wenn sie ans Internet angeschlossen sind - von öffentlichem Interesse ist, wurde parallel die lokale Presse informiert. Dieses Thema steht im gleichen Kontext wie die Berichterstattung über den [Stuxnet-Wurm](https://de.wikipedia.org/wiki/Stuxnet) oder das [Mirai Bot-Netz](https://de.wikipedia.org/wiki/Mirai_(Malware)).

Sowohl unser Anliegen, dem Betreiber das Schließen der Sicherheitslücke zu ermöglichen bevor schlimmeres passiert, als auch eine öffentliche Wahrnehmung der Thematik haben wir erreicht.

Wie reagiert die Stadt auf unsere Mail? Uns liegt keine direkte Antwort der Stadt vor. Aus dem [Artikel](http://www.schwarzwaelder-bote.de/inhalt.horb-a-n-hacker-risiko-oder-kriminelle-fake-news.88e79507-6dfc-42ad-8209-7bcd260f534b.html) erfahren wir, dass wir angeblich Fake News generieren und Sensationen erhaschen wollen. Außerdem hätte unser Mitglied möglicherweise kriminelle Energie. Wir weisen diese Behauptungen entschieden von uns.

![Screenshot der IP bei Shodan](/assets/blog/2017-fake-horb/shodan.png)

Ist unsere Mitteilung Fake News?

Nein, wir sind überzeugt, keine Falschmeldung lanciert zu haben. Die Ports waren tatsächlich erreichbar. Ob es sich, wie von der Stadt dargestellt, um ein "Testsystem" handelt, hinterfragen wir lieber nicht. Auch die Argumentation, es wäre eine Standalone-Anlage, rechtfertigt nicht, den Schutz derselben so zu vernachlässigen.

Sind wir scharf auf Sensationen?

Nein, wir möchten zu einem höheren Sicherheitsbewusstsein beitragen. Aktuell gibt es nahezu täglich Meldungen über [offene Industrieanlagen](https://suche.golem.de/search.php?l=10&q=internetwache), [gestohlene Zugangsdaten](https://haveibeenpwned.com/) und veraltete [unsichere Serversoftware](https://www.heise.de/security/). Hinter all dem stehen Menschen, die - ein entsprechendes Bewusstsein vorausgesetzt - für mehr Sicherheit sorgen können.

Haben wir kriminelle Ernergie?

Nein, es liegt uns fern Schaden anzurichten. Menschen mit krimineller Energie nutzen Schwachstellen und Sicherheitslücken aus, anstatt die Betreiber der Anlage darüber zu informieren, so wie wir es taten. In anderen Fällen konnten durch Hinweise von Vereinsmitgliedern bereits ähnliche Anlagen gesichert werden. In einem weiteren Fall wurde in Zusammenarbeit mit dem [CERT-Bund](https://www.cert-bund.de/) ein offen zugänglicher Dateiserver mit tausenden persönlichen Daten einer Fondsvermittlung vom Netz genommen.

Wir sind überrascht von der Reaktion und den Argumenten der Stadt Horb und stehen einem klärenden Gespräch offen gegenüber.
