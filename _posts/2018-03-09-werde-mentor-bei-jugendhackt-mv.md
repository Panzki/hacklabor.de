---
title: "Werde Mentor*in bei Jugend hackt MV!"
tags: jugendhackt
author: micha
image: /assets/blog/jugendhackt/jh-mentor.png
---
Willkommen 2018! In diesem Jahr treffen sich rund 30 programmierbegeisterte 
Jugendliche in Schwerin, um mit Code die Welt zu verbessern. Vom 01. bis 03. 
Juni 2018 startet Jugend hackt das erste mal in Mecklenburg-Vorpommern.

![Werde Mentor bei Jugend Hackt MV](/assets/blog/jugendhackt/jh-mentor.png)

An drei spannenden und ereignisreichen Tagen haben die Jugendlichen Zeit, 
gemeinsam mit einem kleinen Team, eine eigene digitale Anwendung zu entwickeln 
(die definitiv nicht fertig werden muss!), oder an einem Hardwareprojekt zu 
werkeln. Gemeinsam mit anderen Interessierten haben sie die Möglichkeit etwas 
Neues auszuprobieren und dazuzulernen.

Unterstützt werden sie bei [Jugend hackt in Schwerin](https://jugendhackt.org/events/schwerin/) von tollen, technisch-fitten 
Mentor*innen und einem begeisterten Orga-Team, die sich schon jetzt wahnsinnig 
auf das Event freuen.

* Du hast Erfahrung mit Technik, Design oder Pädagogik? 
* Du bist älter als 19 Jahre? 
* Du willst Jugendliche dabei unterstützen mit Code die Welt zu verbessern?

__Dann werde Mentor*in bei Jugend hackt MV!__

Weitere Infos gibt es auf [https://jugendhackt.org/mitmachen/mentorin/](https://jugendhackt.org/mitmachen/mentorin/). 
Im Vorfeld der Veranstaltung wird es ein Vernetzungtreffen zum Kennenlernen und 
eine Schulung geben.

Du hast Lust? Dann melde dich unter: mv@jugendhackt.org

[Flyer als PDF](/assets/blog/jugendhackt/mentorinnen-flyer.pdf)

Jugend hackt MV wird vom [Landesjugendring e.V.](http://jugendhacktmv.ljrmv.de/), dem [Landesbeauftragtem 
für Datenschutz und Informationsfreiheit](https://www.datenschutz-mv.de/) und dem 
[Hackspace Schwerin e.V.](https://hacklabor.de) in Kooperation mit der [Open Knowledge Foundation](https://okfn.de/) und 
[mediale pfade.org](http://www.medialepfade.org/) veranstaltet.
