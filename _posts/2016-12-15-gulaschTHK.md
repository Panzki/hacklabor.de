---
title: Wir testen Gulasch Rezepte
tags: gulasch, event, oeffentlich
author: Thomas
---

Die uralte und wichtige Frage des: 'Was ist ein gutes Gulasch-Rezept' muss mit einem Testlauf iteriert werden. 
Nach dem ersten erfolgreichen Testlauf am 18. November 2016. Nun der zweite Test um das Ergebniss zu verifizieren ![](/assets/blog/gulasch/Gulaschgut.jpg)

__Deshalb laden wir dich am Freitag, den 16. Dezember ab 13:37 Uhr zum zweiten Gulasch-Koch-und-Test Event ein. ~1800 Wenn du 'nur' Testen willst.__

Wer kein Fleisch mag kann auch Tomentsauce zu den Nudeln bekommen.

Wir freuen uns auf Dich!

PS: [So kommst du zu uns](/standort/)

