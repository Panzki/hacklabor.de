---
title: Opennet in Ludwigslust
tags: opennet, freifunk
author: Rene
---
am Freitag ging es für Opennet nach Ludwigslust. Geplant war der Aufbau zweier NanoStation M5 HP auf dem Dach des Landratsamtes Ludwigslust. Eine in Richtung Westen und eine in Richtung Norden. Dort hatte DSL-Spender Murdoc bereits eine Gegenstelle installiert. Begleitet und dokumentiert wurden die Arbeiten durch ein Kamerateam vom NDR.
![Dach](/assets/blog/2017-03-29-opennet/opennet-lwl2.jpg)

So ging es also mit Sack und Pack auf den Dachboden vom Landratsamt. Die Antennen konnten in direkter Reichweite vom Dachausstieg montiert werden. So entfiel ein riskanter Aufstieg auf den 10m hohen Mast. Auch eine Kabeldurchführung unters Dach war bereits vorbereitet. Beide Antennen sind nun über den Secondary Port verbunden und so konnte ohne Switch das OLSR Signal direkt zum nächsten UGW geführt werden. Dieses UGW befindet sich direkt eine Etage tiefer in einem Serverraum. Über einen vorhandenen DSL Anschluss ergibt sich darüber der Zugang zum Internet. So brauchte nur noch ein LAN Kabel nach unten gezogen werden.

![Antenne](/assets/blog/2017-03-29-opennet/opennet-lwl1.jpg)

Nach Abschluss der groben Arbeiten erfolgte noch das Crimpen zweier Stecker, das Ausrichten der Antennen und eine leichte Korrektur der verlegten LAN Kabel. Die erste aktive Funkverbindung über knapp 1km mit dem zweiten UGW ist nun Dank Murdoc hergestellt und ergibt somit bessere Ausfallsicherheit.

Gegen Mittag erfolgte dann die Heimfahrt nach Schwerin.

Für Ludwigslust sind nun die ersten beiden 5 Ghz Accesspoints aktiv und bieten nun die Möglichkeit das Netz von vielen Standorten zu erweitern.

Berichterstattung:

* [Fernsehbeitrag im NDR](http://www.ndr.de/fernsehen/sendungen/nordmagazin/Opennet-Engagiert-fuer-freies-WLAN,nordmagazin41092.html)
* [Artikel in der SVZ](http://www.svz.de/lokales/ludwigsluster-tageblatt/offenem-netz-ein-stueck-naeher-id16377086.html)
* [Bericht vom Dreh im Podcast der Flachlandreporter](http://www.flachlandreporter.de/flrp012-alte-kulturwuerfel/#t=41:32.588)
