---
title: Binäre Zahlenwerte. Vom Bit zum Real bzw Float
tags: Technik, event, oeffentlich
author: Thomas
---
![](/assets/blog/Veranstaltung/BinaereZahlen.PNG)

#### Wer Lust hat kommt am 29.01.2017 um 14:00 Uhr ins Hacklabor und wir reden gemeinsam über

* Aufbau von Binären Zahlen
* Konvertierung DEZ <--> Binär
* Addition, Substraktion, Multiplikation
* logische digitale Operationen 

#### Voraussetzung

* Spaß am lernen

Wir freuen uns auf Dich!

PS: [So kommst du zu uns](/standort/)