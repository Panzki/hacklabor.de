---
title: Layer 1 Kommunikation - wo Information zu Elektronen und Elektronen zu Information wird
tags: Technik, event, oeffentlich
author: Thomas
---
![](/assets/blog/Veranstaltung/Layer1Kommunikation.png)

#### Wer Lust hat kommt am 05.02.2017 um 14:00 Uhr ins Hacklabor und wir reden gemeinsam über

* RS232, RS458, Ethernet, I2C, SPI, 4...20mA, 0-10V
* Unterschiede und Gemeinsamkeiten
* Pegel; Signalkodierung
* Synchronisation SCK, SCL…...

#### Voraussetzung

* Spaß am lernen

Wir freuen uns auf Dich!

PS: [So kommst du zu uns](/standort/)
