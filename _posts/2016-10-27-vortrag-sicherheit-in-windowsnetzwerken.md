---
title: 'Vortrag: 01.11.2016 - Fenster einschlagen für Dummys'
tags: vortrag, windows
author: 'rabbit@net'
---

Spaß in Windows-basierten Netzwerken - und was man dagegen tun kann

Microsoft Windows ist nach wie vor ein verbreitetes Betriebssystem in Unternehmen, 
häufig in Kombination mit dem Verzeichnisdienst Active Directory. Dabei 
stellt der sichere Betrieb einer solchen Umgebung eine Herausforderung dar. 
Häufig benötigt man keinen ausgefallen Exploit, um seine Rechte im 
Unternehmensnetzwerk auszuweiten. 

Im Vortrag soll es um häufige Informationslecks und Fehlkonfigurationen gehen, 
wie man diese findet, ausnutzt und etwas dagegen tun kann.

Ein Vortrag von unserem Mitglied Rabbit@Net

Der Eintritt zu dieser öffentlichen Veranstaltung ist frei.

01.11.2016 - Beginn 1930 Uhr  
Hacklabor - Hagenower Straße 73 - 19061 Schwerin  
https://hacklabor.de/standort/