---
title: Passive Bauelemente im Gleichstromkreis
tags: Technik, event, oeffentlich
author: Thomas
---
![](/assets/blog/passive_Bauelemente/IMG_20161121_194418.jpg)

Wer mehr über die technischen Zusammenhänge von Widerstand, Strom, Spannung und Leistung
im Gleichstromkreis erfahren möchte, kommt am 08.01.2017 um 14.00 Uhr ins Hacklabor.

_Voraussetzung_

* Mathematik Klasse 7
* Taschenrechner
* Spaß am lernen

Wir freuen uns auf Dich!

PS: [So kommst du zu uns](/standort/)