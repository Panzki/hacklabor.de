---
layout: page
title: Projekte
permalink: /projekte/
---

Diese Liste ist immer unvollständig und nicht aktuell, gibt aber einen kleinen Einblick hinter die Kulissen.

<div class="card-columns">
{% for projekt in site.data.projekte %}

  <div class="card">
    <!-- img class="card-img-top" data-src="..." alt="Card image cap" -->
    <div class="card-block">
      <h4 class="card-title">{{ projekt.name }}</h4>
      <p class="card-text">{{ projekt.desc }}</p>
      <a class="btn btn-primary" href="{{ projekt.link }}">Details</a>
    </div>
  </div>
  
{% endfor %}
</div>

## Projekte auf gitlab.com

<div class="card-columns">
{% for projekt in site.data.gitlabprojects %}
  {% if projekt.public %}
    <div class="card">
      <!-- img class="card-img-top" data-src="..." alt="Card image cap" -->
      <div class="card-block">
        <h4 class="card-title">{{ projekt.name }}</h4>
        <p class="card-text">{{ projekt.description }}</p>
        <a class="btn btn-primary" href="{{ projekt.web_url }}">Details</a>
        {% if projekt.wiki_enabled %}
          <a class="btn btn-primary" href="{{ projekt.web_url }}/wikis/home">Wiki</a>
        {% endif %}
      </div>
    </div>
  {% endif %}
{% endfor %}
</div>
