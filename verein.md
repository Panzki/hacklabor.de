---
layout: page
title: Hackspace Schwerin e.V.
permalink: /verein/
---

Das Hacklabor wird vom Verein Hackspace Schwerin e.V. betrieben. Die Gründung des Vereins erfolgte am 26. Februar 2016. Beim Amtsgericht Schwerin hat der Verein die Registernummer 10234. Die [Satzung](/satzung.pdf) kannst du [hier](/satzung.pdf) nachlesen.

## Vorstand

Der Vorstand besteht aus drei Personen:

-   [Murdoc Bates](https://twitter.com/trockenasche)
-   [Michael Milz](https://www.xing.com/profile/Michael_Milz2)
-   [Matthias Manow](https://twitter.com/moeses)

{% include mitglied-werden.html %}

## Spenden

Der Verein ist gemeinnützig und Spenden an den Verein können steuerlich geltend gemacht werden.

Zur Unterstützung gibt es ein Amazon Affliate Link [amazon.hacklabor.de](http://amazon.hacklabor.de)


Zusätzlich kann man uns über die [Kaufkröte](http://www.kaufkroete.de/4224spenden) unterstützen [kaufkroete.de/4224spenden](http://www.kaufkroete.de/4224spenden) auf der man noch weitere Shops besuchen kann. Dafür bekommen wir Provision ohne das sich der Kaufpreis erhöht.

Außerdem kann man unsere [Wünsche](https://www.amazon.de/gp/registry/wishlist/2W8SE6FZMKAZE/ref=as_li_ss_tl?ie=UTF8&linkCode=ll2&tag=hacklabor-21&linkId=126aad26a1031ff4751c1fe3e060ea76) bei Amazon erfüllen.

## Kontoverbindung

Kontoinhaber: Hackspace Schwerin e.V.  
IBAN: DE76 8306 5408 0004 9491 70

## Sponsoring

Bitte [sprechen Sie uns an](/kontakt/).

## Logos

![Kleines Logo Hacklabor](/assets/img/logo/Logo_small_white.svg.png)  

-   PNG [weiß auf transparent](/assets/img/logo/Logo_small_white.svg.png) / [schwarz auf weiß](/assets/img/logo/Logo_small_black.svg.png)
-   SVG [weiß auf transparent](/assets/img/logo/Logo_small_white.svg) / [schwarz auf weiß](/assets/img/logo/Logo_small_black.svg)

![Mittleres Logo Hacklabor](/assets/img/logo/Logo_medium_white.svg.png)  

-   PNG [weiß auf transparent](/assets/img/logo/Logo_medium_white.svg.png) / [schwarz auf weiß](/assets/img/logo/Logo_medium_black.svg.png)
-   SVG [weiß auf transparent](/assets/img/logo/Logo_medium_white.svg) / [schwarz auf weiß](/assets/img/logo/Logo_medium_black.svg)

![Großes Logo Hacklabor](/assets/img/logo/Logo_Large_white.svg.png)  

-   PNG [weiß auf transparent](/assets/img/logo/Logo_Large_white.svg.png) / [schwarz auf weiß](/assets/img/logo/Logo_Large_black.svg.png)
-   SVG [weiß auf transparent](/assets/img/logo/Logo_Large_white.svg) / [schwarz auf weiß](/assets/img/logo/Logo_Large_black.svg)
